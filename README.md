### confluence-jira
[![Docker Image CI](https://github.com/vncloudsco/confluence-jira/actions/workflows/docker-image.yml/badge.svg?branch=7.16.2-8.22.2)](https://github.com/vncloudsco/confluence-jira/actions/workflows/docker-image.yml)

Đây là bản mới nhất của jira và confluence đã được cấu hình hoàn chỉnh 

### chạy phần mềm trên x86_64

```
git clone https://github.com/vncloudsco/confluence-jira.git
cd confluence-jira
docker-compose up -d
``` 
### chạy phần mềm trên ARM

```
git clone https://github.com/vncloudsco/confluence-jira.git
cd confluence-jira
docker-compose -f docker-compose-arm.yaml up -d
``` 

sau khi chạy thì vào các port tương ứng để cấu hình phần mềm trên máy chủ

| Phần mềm     | Port        | dbhost       | db port | db Type  |   database info in file | Version |
| ------------ | ----------- | ------------ | ------- | -------  | ----------------------- | --------|
| jira         | 8080        | jiradb       |  5432   | postgres | database-jira.env       |    13   |
| confluence   | 8090        | confluencedb |  5432   | postgres | database-confluence.env |    14   |


thông tin kết nối db xem trong file  ```database-confluence.env``` và ```database-jira.env``` và điền vào các ô tương ứng khi cài đặt

- lưu ý: 
    - hệ thống sẽ dùng postgres
    - thông tin db trong file env tương ứng


để crack thì chạy lệnh sau 

- lưu ý 
    - nhớ thay 127.0.0.1 thành IP/domain của bạn,
    - confluence thì không cần quan tâm server ID cứ lệnh đó mà chạy
    - jira thì nhớ thay server ID 

jira keygen

```

docker exec jira-srv java -jar /var/agent/atlassian-agent.jar \
    -p jira \
    -m admin@gmail.com \
    -n admin@gmail.com \
    -o http://127.0.0.1:8080 \
    -s BOQU-95EE-NRW6-AQDG

```

confluence keygen

```
docker exec confluence-srv java -jar /var/agent/atlassian-agent.jar \
    -p conf \
    -m admin@gmail.com \
    -n admin@gmail.com \
    -o http://127.0.0.1:8090 \
    -s 202
```
### Cầu hình domain 

Để cấu hình doamin thì vào file server.xml trong thư mục tương ứng để sửa đổi thông tin theo yêu cầu của bạn, bạn nên dùng nginx manager proxy là proxy để dễ dàng quản lý máy chủ, tùy vào bạn muốn dùng https hay http mà bỏ comment phần connet tương ứng, lưu ý phần Connector mặc định cần được comment lại hoặc loại bỏ để tránh xung độ

| Phần mềm     | File Config   |
| ------------ | ---------    |
| confluence   | confluence/server.xml   |
| Jira         | Jira/server.xml   |



Phiên bản hỗ trợ

| Phần mềm     | Version   | x86_64 | ARM |
| ------------ | --------- | ------ | --- |
| confluence   | 7.16.2    | Yes    | Yes |
| jira         | 8.22.2    | Yes    | Yes |


### Cấu hình bẻ khóa các plugin trên marketplace

- sau khi tải về marketplace các bạn truy cập vào  Manage apps của Jira và Confluence và chọn addon cần bẻ khóa
- Trong giao diện mới hiện ra có phần App key các bạn lấy App key này để làm bước tiếp 
- để bẻ khóa addon của jira

```

docker exec jira-srv java -jar /var/agent/atlassian-agent.jar \
    -p < thay App key của bạn vào đây > \
    -m admin@gmail.com \
    -n admin@gmail.com \
    -o http://127.0.0.1:8080 \
    -s BOQU-95EE-NRW6-AQDG

```

- để bẻ khóa addon của confluence

```
docker exec confluence-srv java -jar /var/agent/atlassian-agent.jar \
    -p < thay App key của bạn vào đây > \
    -m admin@gmail.com \
    -n admin@gmail.com \
    -o http://127.0.0.1:8090 \
    -s 202
```

### sửa lỗi không lưu được bài trên confluence arm

Đầu tiên truy cập vào General Configuration  >  Collaborative editing trong cài đặt chuyển Collaborative editing của Editing mode về thành off. Quay lại trang chủ bạn có thể đăng bài bình thường tuy nhiên sẽ mất tính năng autosvae, các bạn phải thao tác lưu thủ công
